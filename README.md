
See [frontend/README.md](frontend/README.md)

Delete the `.gitignore` in this directory. It is used by this repo only, and is not required for projects using it.
